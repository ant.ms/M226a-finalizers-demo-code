﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finalizers_Test {
    class Program {
        static void Main(string[] args) {
            /// !!!!! LAUCH THIS CODE WITH CRTL + F5 !!!!!

            // create object
            B myObject = new B();

            // "remove" object
            myObject = null;

            //// display how much memory is being used (so that we can compare later)
            Console.WriteLine("Memory used before garbage collection:\t" + GC.GetTotalMemory(false));

            // call Garbage Collection manually
            GC.Collect();

            // Wait until all Finalizers have finished executing
            GC.WaitForPendingFinalizers();

            //// display how much memory is being used (so that we can see, that the object is actually being removed)
            Console.WriteLine("Memory used before garbage collection:\t" + GC.GetTotalMemory(true));
        }
    }

    class A {
        public A() {
            Console.WriteLine("Constructor for A has been called");
        }
        ~A() {
            Console.WriteLine("Deconstructor for A has been called");
        }
    }

    class B : A {
        public B() {
            Console.WriteLine("Constructor for B has been called");
        }
        ~B() {
            Console.WriteLine("Deconstructor for B has been called");
        }
    }
}
